syntax enable
set number
set expandtab
set softtabstop=4
set tabstop=4

set viminfo='10,\"100,:20,%,n~/.viminfo
  au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm$"|endif|endif
 
set nocompatible
syntax on
set nobackup
"colorscheme desertEx
"colorscheme solarized
"set background=dark
set showmatch
