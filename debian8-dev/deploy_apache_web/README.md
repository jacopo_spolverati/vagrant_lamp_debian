Ruolo per creare vhost apache e relativa docroot con acl corrette per progetti sf2

Varibili utilizzate :

 task/add_vhost_dir.yml

  vhostpath
  vhostdir
  vhostowner
  vhostgroup
  wwwuser
  wwwgroup
  aliases[]  
  setacl

 task/main.yml 
  vhostfile 

 templates/vhost.j2
  servername 
