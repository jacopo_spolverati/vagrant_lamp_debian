## Vagrant file e ansible playbook per LAMP  debian 8 "jessie" , php 5.6, apache 2.4


Installare 

- Virtualbox
- vagrant 1.7.3

La configurazione si basa su questa box:
https://atlas.hashicorp.com/debian/boxes/jessie64/versions/8.1.0
Debian jessie ufficiale.
Si crea un ambiente di sviluppo per il progetto sf2 del sito beelab.net con i sorgenti in una cartella condivisa con l'host

Dopo aver clonato questa repository 

```sh
$ cd vagrant_lamp_debian/prj

$ git clone git@bitbucket.org:beelab/beelab.git

$ vagrant up 
```

La VM vagrant ha le sue configurazioni su un file di configurazine separato da Vagrantfile, il file config.xml:

- ip privato 192.168.111.2

- shared dir: 
	default /vagrant su guest -> root del progetto su host
	/var/www/vhosts/beelab/current su guest -> prj/beelab su host

- port forward 80 su 8080 locale.


Vagrant esegue  dei playbook di Ansible per creare un ambiente dev per php su cui è installato:

- php 5.6

- mysql 5.5

- apache 2.4

- composer 

- bowerphp

- phpunit

- phpmd


Viene creato un vhost di apache con mod_php configurato per sf2.
La docroot punta su /var/www/vhosts/beelab/current/web - la cartella condivsa del guest - 

I vari comandi app/console per sf2 vanno eseguiti dentro la VM

```sh
vagrant ssh
```
